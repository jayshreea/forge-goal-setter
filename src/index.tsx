import ForgeUI, {
  render,
  useConfig,
  Fragment,
  Text,
  TextField,
  Select,
  Option,
  ConfigForm,
  Macro,
  Image,
} from "@forge/ui";

import moment from "moment";
import { daysTill, generateDataURI } from "./util";

const defaultConfig = {
  time: moment().format("YYYY-MM-DD"),
};

const Config = () => {
  return (
    <ConfigForm>
      <TextField
        label="Input Date (YYYY-MM-DD)"
        name="time"
        defaultValue={moment().format("YYYY-MM-DD")}
      />
    </ConfigForm>
  );
};

const App = () => {
  const { tz, time } = useConfig();
  const daysLeft = daysTill(time);
  const dataURI = generateDataURI(daysLeft);

  return (
    <Fragment>
      <Text>Hello world!</Text>
      <Fragment>
        <Image src={dataURI} alt={daysLeft} />
      </Fragment>
    </Fragment>
  );
};

export const run = render(
  <Macro app={<App />} config={<Config />} defaultConfig={defaultConfig} />
);
